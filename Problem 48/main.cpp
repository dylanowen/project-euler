/*
I'm fairly certain this has horrible memory management... That's kind of a big problem
*/


#include <cstdio>
#include <iostream>
#include <string>
#include <sstream>

#include "SuperLong.h"
#include "Timer.h"

using namespace std;

SuperLong* power(SuperLong*, SuperLong*);

int main(int argc, const char *argv[])
{
	Timer *ct;
	ct = new Timer();

	SuperLong *sum = new SuperLong(), *temp, *input;
	
	for(int i = 1; i <= 1000; i++)
	{
		input = new SuperLong((long long) i);
		temp = power(input, input);
		*sum += *temp;
		delete temp;
		delete input;
		cout << *sum << endl;
	}
	
	cout << "Sum: " << *sum << endl;
	delete sum;
	cout << "Ran in REAL: " << ct->format(ct->timeReal()) << " CPU: " << ct->timeCPU() << endl;
	delete ct;
	return 1;
}//main()

SuperLong* power(SuperLong* num, SuperLong* power)
{
	SuperLong *answer = new SuperLong((long long) 1), *i;
	
	for(i = new SuperLong(); *i < *power; (*i)++)
	{
		*answer *= *num;
	}
	
	delete i;
	return answer;
}