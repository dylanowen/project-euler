#include "Timer.h"

Timer::Timer()
{
	reset();
}

string Timer::format(double val)
{
	stringstream ss;
	long temp2, temp = (long) val;
	temp2 = temp / (60 * 60 *24);
	if(temp2 != 0)
	{
		ss << temp2 << " Days ";
	}
	temp %= (60 * 60 * 24);
	ss << temp / (60 * 60) << ":";
	temp %= (60 * 60);
	ss << setfill('0') << setw(2) << temp / (60) << ":";
	temp %= (60);
	ss << setfill('0') << setw(2) << temp;
	return ss.str();
}

void Timer::reset()
{
	resetReal();
	resetCPU();
}

void Timer::resetReal()
{
	countReal = time(NULL);
}

void Timer::resetCPU()
{
	countCPU = clock();
}

double Timer::timeReal()
{
	return difftime(time(NULL), countReal);
}

double Timer::timeCPU()
{
	return double(clock() - countCPU) / CLOCKS_PER_SEC;
}