#pragma once

#include <iostream>
#include <vector>

using namespace std;

class SuperLong
{
		vector<char> number;
		bool sign; //true is negative false is positive
		
	public:
		SuperLong();
		SuperLong(long long num);
		SuperLong(unsigned long long num);
		SuperLong(string num);
		
		bool operator==(const SuperLong &rhs) const;
		bool operator!=(const SuperLong &rhs) const;
		bool operator<(const SuperLong &rhs) const;
		bool operator>(const SuperLong &rhs) const;
		bool operator<=(const SuperLong &rhs) const;
		bool operator>=(const SuperLong &rhs) const;
		
		const SuperLong operator+(const SuperLong &rhs) const;
		const SuperLong operator-(const SuperLong &rhs) const;
		const SuperLong operator*(const SuperLong &rhs) const;
		//const SuperLong operator/(const SuperLong &rhs) const;
		//const SuperLong operator%(const SuperLong &rhs) const;

		const SuperLong& operator++();
		const SuperLong operator++(int);
		const SuperLong& operator--();
		const SuperLong operator--(int);
		
		
		SuperLong& operator+=(const SuperLong &rhs);
		SuperLong& operator-=(const SuperLong &rhs);
		SuperLong& operator*=(const SuperLong &rhs);
		//SuperLong& operator/=(const SuperLong &rhs);
		//SuperLong& operator%=(const SuperLong &rhs);

		SuperLong& operator=(const SuperLong &rhs);

		unsigned long long size();
		
		friend ostream& operator<<(ostream &os, const SuperLong &super);
};
