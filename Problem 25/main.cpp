#include <iostream>
#include <cstdlib>
#include <climits>
#include "CPUTimer.h"
#include "superLong.h"

using namespace std;

int numLength(unsigned long long num)
{
	int len = 0;
	while(num > 0)
	{
		num /= 10;
		len++;
	}
	return len;
}

int main(int argc, const char *argv[])
{
	CPUTimer ct;

	SuperLong current((long long) 1), last((long long) 0), temp;
	int i = 0;

	while(current.size() < 1000)
	{
		temp = current;
		current += last;
		last = temp;

		i++;
	}

	cout << i + 1 << endl;

	cout << "CPU time: " << ct.cur_CPUTime() << endl;
}