#include <cstdio>
#include <iostream>
#include <string>
#include <sstream>

using namespace std;

int getPaths(int x, int y)
{
	if(x == 0 || y == 0)
		return 1;

	//cout << x << ":" << y << endl;

	return getPaths(x - 1, y) + getPaths(x, y - 1);
}


int main(int argc, const char *argv[])
{
	
	cout << getPaths(20, 20) << endl;



	return 1;
}//main()