#include <iostream>
#include "CPUTimer.h"

using namespace std;

int main(int argc, const char *argv[])
{
	CPUTimer ct;
	unsigned int total = 0, temp, prev = 0, num = 1;
	while(num < 4000000)
	{
		cout << num << endl;
		temp = num;
		num += prev;
		prev = temp;
		if(num % 2 == 0)
			total += num;
	}
	cout << endl << total << endl;
	cout << "CPU time: " << ct.cur_CPUTime() << endl;
}//main()