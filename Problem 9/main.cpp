#include <cstdio>
#include <vector>
#include "CPUTimer.h"

using namespace std;

int main(int argc, const char *argv[])
{
	CPUTimer ct;
	vector<int> num(1000);
	vector<int*> temp;
	unsigned int i, j, k;
	int *t;
	for(i = 0; i < num.size(); i++)
	{
		num[i] = i * i;
	}
	for(i = 0; i < num.size(); i++)
	{
		for(j = i + 1; j < num.size(); j++)
		{
			for(k = j + 1; k < num.size(); k++)
			{
				if(i + j + k == 1000)
				{
					t = new int[3];
					t[0] = i;
					t[1] = j;
					t[2] = k;
					temp.push_back(t);
				}
			}
		}
	}
	for(i = 0; i < temp.size(); i++)
	{
		if((temp[i][0] * temp[i][0]) + (temp[i][1] * temp[i][1]) == (temp[i][2] * temp[i][2]))
		{
			cout << temp[i][0] << " " << temp[i][1] << " " << temp[i][2] << endl;
			cout << temp[i][0] * temp[i][1] * temp[i][2] << endl;
		}
	}
	cout << "CPU time: " << ct.cur_CPUTime() << endl;
}//main()