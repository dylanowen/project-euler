import Debug.Trace

change :: [Int] -> Int -> Int

change [] _ = 0
change _  0 = 1
change coins remainder = --traceShow (coins, remainder) $
   if remainder < 0 then 0
   else if remainder == 0 then 1
   else do
      let newRemainder = remainder - (head coins)
      change coins newRemainder + change (tail coins) remainder


main = print(change [200,100,50,20,10,5,2,1] 200)