#include <cstdio>
#include <iostream>
#include <iomanip> 

using namespace std;

struct array{
  unsigned long long length;
  unsigned long long* data;
};

array findPrimes();

bool inline checkPermutation(int chars1[4], int chars2[4])
{
	bool tempMatch;
	int charsTemp[4];

	for(int i = 0; i < 4; i++)
	{
		charsTemp[i] = chars2[i];
	}

	for(int i = 0; i < 4; i++)
	{
		tempMatch = false;
		for(int j = 0; j < 4 && tempMatch == false; j++)
		{
			if(chars1[i] == charsTemp[j])
			{
				tempMatch = true;
				charsTemp[j] = -1;
			}
		}
		if(!tempMatch)
		{
			return false;
		}
	}

	return true;
}

void inline getChars(unsigned long long num, int chars[4])
{
	int i = 3;

	while(num > 0)
	{
		chars[i--] = num % 10;
		num /= 10;
	}
}

int main(int argc, const char *argv[])
{
	array primes;
	int charsTemp[4], test[4];
	
	primes = findPrimes();

	for(unsigned int i = 0; i < primes.length; i++)
	{
		unsigned long long tempAnswer[20], tempDifference[20 * 20] = {0};
		int count = 0, numMatches, maxMatches = 0;

		tempAnswer[count++] = primes.data[i];

		//cout << primes.data[i] << endl;

		getChars(primes.data[i], charsTemp);
		for(unsigned int j = i + 1; j < primes.length; j++)
		{
			getChars(primes.data[j], test);			

			if(checkPermutation(charsTemp, test))
			{
				tempAnswer[count] = primes.data[j];
				//tempDifference[count] = tempAnswer[count] - tempAnswer[count - 1];
				count++;
				//cout << count++ << " " << primes.data[i] << ":" << primes.data[j] << " " << primes.data[j] - primes.data[i] << endl;
			}
		}

		for(int i = 0; i < count; i++)
		{
			for(int j = i + 1; j < count; j++)
			{
				tempDifference[i * 20 + j] = tempAnswer[j] - tempAnswer[i];
			}
		}

		for(int i = 0; i < count; i++)
		{
			for(int j = i + 1; j < count; j++)
			{
				numMatches = 1;
				
				for(int k = j + 1; k < count; k++)
				{
					if(tempDifference[i * 20 + j] == tempDifference[j * 20 + k])
					{
						numMatches++;
					}
				}

				if(numMatches > maxMatches)
					maxMatches = numMatches;
			}
		}

		if(maxMatches > 1 || tempAnswer[0] == 1487)
		{
			cout << maxMatches << endl;
			for(int i = 0; i < count; i++)
			{
				cout << tempAnswer[i] << " ";
			}
			cout << endl;

			for(int i = 0; i < count; i++)
			{
				for(int j = 0; j < count; j++)
				{
					cout << setw(4) << tempDifference[i * 20 + j] << " ";
				}
				cout << endl;
			}
			cout << endl;
		}
		

/*

		for(int j = 1; j < count; j++)
		{
			numMatches = 0;
			for(int k = j + 1; k < count; k++)
			{
				cout << tempDifference[i] << ":" << tempDifference[j]
				if(tempAnswer[i] == tempDifference[j])
				{
					numMatches++;
				}
			}
		}

		if(numMatches > maxMatches)
		{
			maxMatches = numMatches;
		}

		cout << maxMatches << endl;

		

		if(count >= 3)
		{
		for(int j = 0; j < count; j++)
		{
			cout << tempDifference[j] << " ";
		}
		cout << endl;
		for(int j = 0; j < count; j++)
		{
			cout << tempAnswer[j] << " ";
		}
		cout << endl << endl;
	}
		for(int j = 1; j < count; j++)
		{
			for(int k = 1; k < count; k++)
			{

			}

			if(tempDifference[j] != tempDifference[j - 1])
			{
				check = false;
			}
		}
			if(check)
		{
			answer[0] = tempAnswer[0];
			answer[1] = tempAnswer[1];
			answer[2] = tempAnswer[2];
		
			for(int j = 0; j < count; j++)
			{
				//cout << tempAnswer[j] << " ";
			}

			//cout << endl;

			//cout << answer[0] << " " << answer[1] << " " << answer[2] << endl;
		}
		*/
	}

	
	
	
	return 0;
}//main()

array findPrimes()
{
	unsigned long long j, upTo = 10000, count = 0;
	bool *temp = new bool[upTo];
	array primes;
	
	for(unsigned long long i = 0; i < upTo; i++)
	{
		temp[i] = true;
	}
	
	temp[0] = false; //0 isn't prime
	temp[1] = false; //1 isn't prime
	
	for(unsigned long long i = 2; i < ((upTo / 2) + 1); i++)
	{
		if(temp[i])
		{
			j = i + i;
			while(j < upTo)
			{
				temp[j] = false;
				j += i;
			}
		}
	}
	
	for(unsigned long long i = 1000; i < upTo; i++)
	{
		if(temp[i])
		{
			count++;
		}
	}
	
	primes.length = count;
	primes.data = new unsigned long long[count];
	j = 0;
	
	for(unsigned long long i = 1000; i < upTo; i++)
	{
		if(temp[i])
		{
			primes.data[j] = i;
			j++;
		}
	}
	
	return primes;
}