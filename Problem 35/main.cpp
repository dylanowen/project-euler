#include <cstdio>
#include <iostream>
#include <sstream>
#include <string>
#include <stdlib.h>

#include "Timer.h"

using namespace std;

int main(int argc, const char *argv[])
{
	stringstream *ss;
	string temp;
	bool test;
	
	Timer *ct;
	ct = new Timer();
	
	unsigned int j, count = 0, upTo = 1000000;
	bool *primes = new bool[upTo];
	
	for(unsigned int i = 0; i < upTo; i++)
	{
		primes[i] = true;
	}
	
	primes[0] = false; //0 isn't prime
	primes[1] = false; //1 isn't prime
	
	for(unsigned int i = 2; i < ((upTo / 2) + 1); i++)
	{
		if(primes[i])
		{
			j = i + i;
			while(j < upTo)
			{
				primes[j] = false;
				j += i;
			}
		}
	}
	
	for(unsigned int i = 0; i < upTo; i++)
	{
		if(primes[i])
		{
			ss = new stringstream();
			*ss << i;
			
			temp = ss->str();
			test = true;
			for(unsigned int j = 0; j < temp.length(); j++)
			{
				temp.push_back(temp[0]);
				temp.erase(0, 1);
				if(!primes[atoi(temp.c_str())])
				{
					test = false;
				}
			}
			
			if(test)
			{
				cout << i << endl;
				count++;
			}
			
			delete ss;
		}
	}
	
	cout << "Count: " << count << endl;
	
	cout << "Ran in REAL: " << ct->format(ct->timeReal()) << " CPU: " << ct->timeCPU() << endl;
	delete ct;
	return 1;
}//main()