#ifndef TIMER_H_
#define TIMER_H_

#include <string>
#include <sstream>
#include <iomanip>
#include <time.h>

using namespace std;

class Timer
{
	private:
		time_t countReal;
		clock_t countCPU;
	public:
		Timer();
		string format(double);
		void reset();
		void resetReal();
		void resetCPU();
		double timeReal();
		double timeCPU();
};

#endif