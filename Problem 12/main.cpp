#include <cstdio>
#include <iostream>

#include "Timer.h"

using namespace std;

int countDivisors(unsigned long long number);

int main(int argc, const char *argv[])
{
	unsigned long long number = 1, tri = 2;
	int divisors, greatest = 0;
	
	Timer *ct;
	ct = new Timer();
	
	do
	{
		number += tri;
		divisors = countDivisors(number);
		if(divisors > greatest)
		{
			greatest = divisors;
		}
		cout << greatest << ": " << number << " => " << divisors << endl;
		tri++;
	
	}while(divisors < 500);
	
	cout << "Number: " << number << endl;
	
	cout << "Ran in REAL: " << ct->format(ct->timeReal()) << " CPU: " << ct->timeCPU() << endl;
	delete ct;
	return 1;
}//main()

int countDivisors(unsigned long long number)
{

	int count = 0;
	
	for(unsigned long long i = 1; i <= ((number / 2) + 1); i++)
	{
		if(number % i == 0)
		{
			count++;
		}
	}
	
	return count + 1;
}