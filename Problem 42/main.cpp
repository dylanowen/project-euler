#include <cstdio>
#include <string.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>

#include "Timer.h"

using namespace std;

bool findTriangle(int triangleNumbers[], string *number);
int convertToInt(string *number);

int main(int argc, const char *argv[])
{
	char temp[256];
	Timer *ct;
	vector<string *> *words = new vector<string *>;
	int triangleNumbers[100];
	ct = new Timer();
	int count = 0;
	
	ifstream *wordFile;
	wordFile = new ifstream("words.txt", ios::in);
	
	while(wordFile->good())
    {
		wordFile->getline(temp, 256, '"');
		wordFile->getline(temp, 256, '"');
		if(strlen(temp) > 0)
		{
			words->push_back(new string(temp));
		}
    }
	
	cout << "total words: " << words->size() << endl; 
	
	for(int i = 0; i < 100; i++)
	{
		triangleNumbers[i] = (i * (i + 1)) / 2;
	}
	
	for(vector<string *>::iterator it = words->begin(); it < words->end(); it++)
	{
		if(findTriangle(triangleNumbers, *it))
		{
			count++;
		}
	}
	
	cout << "triangle numbers: " << count << endl; 
	
	cout << "Ran in " << ct->format(ct->timeReal()) << endl;
	delete ct;
	return 1;
}//main()

bool findTriangle(int triangleNumbers[], string *number)
{
	int temp = convertToInt(number);
	for(int i = 0; i < 100; i++)
	{
		if(triangleNumbers[i] == temp)
		{
			cout << *number << ":" << temp << endl;
			return true;
		}
	}
	return false;
}

int convertToInt(string *number)
{
	int temp = 0;
	for(unsigned int i = 0; i < number->size(); i++)
	{
		temp += (*number)[i] - 'A' + 1;
		//cout << (*number)[i] << " = " << ((*number)[i] - 'A' + 1) << endl;
	}
	return temp;
}