#include <cstdio>
#include <iostream>

#include "Timer.h"

using namespace std;

struct array{
  unsigned long long length;
  unsigned long long* data;
};

array findPrimes(unsigned long long upTo);

int main(int argc, const char *argv[])
{
	array primes;
	unsigned long long number = 600851475143;
	
	Timer *ct;
	ct = new Timer();
	
	primes = findPrimes(10000);
	
	cout << "Total primes: " << primes.length << endl;
	
	for(unsigned long long i = 0; i < primes.length; i++)
	{
		if((number % primes.data[i]) == 0)
		{
			cout << primes.data[i] << endl;
			number = number / primes.data[i];
		}
	}
	
	cout << "Number: " << number << endl;
	
	cout << "Ran in REAL: " << ct->format(ct->timeReal()) << " CPU: " << ct->timeCPU() << endl;
	delete ct;
	return 1;
}//main()

array findPrimes(unsigned long long upTo)
{
	unsigned long long j, count = 0;
	bool *temp = new bool[upTo];
	array primes;
	
	for(unsigned long long i = 0; i < upTo; i++)
	{
		temp[i] = true;
	}
	
	temp[0] = false; //0 isn't prime
	temp[1] = false; //1 isn't prime
	
	for(unsigned long long i = 2; i < ((upTo / 2) + 1); i++)
	{
		if(temp[i])
		{
			j = i + i;
			while(j < upTo)
			{
				temp[j] = false;
				j += i;
			}
		}
	}
	
	for(unsigned long long i = 0; i < upTo; i++)
	{
		if(temp[i])
		{
			count++;
		}
	}
	
	primes.length = count;
	primes.data = new unsigned long long[count];
	j = 0;
	
	for(unsigned long long i = 0; i < upTo; i++)
	{
		if(temp[i])
		{
			primes.data[j] = i;
			j++;
		}
	}
	
	return primes;
}