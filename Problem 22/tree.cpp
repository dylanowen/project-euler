#include "tree.h"

using namespace std;

Tree::Tree()
{
	root = NULL;
}
Tree::~Tree(){}

void Tree::insert(Node *nn)
{
	if(root == NULL)
	{
		root = nn;
		return;
	}
	bool temp = true;
	int height;
	list<Node*> ancestors;
	Node *next = root, *t, **p;
	while(next != NULL)
	{
		ancestors.push_back(next);
		if(nn->name.compare(next->name) <= 0)
		{
			next = next->left;
		}
		else
		{
			next = next->right;
		}
	}//cycle down into the tree
	next = ancestors.back();
	if(nn->name.compare(next->name) <= 0)
	{
		next->left = nn;
	}
	else
	{
		next->right = nn;
	}
	while(!ancestors.empty())
	{
		height = calc_height(ancestors.back());
		if(height != ancestors.back()->height)
		{
			ancestors.back()->height = height;
		}
		else
		{
			temp = false;
		}
		t = ancestors.back();
		ancestors.pop_back();
		if(ancestors.empty())
		{
			p = &root;
		}
		else
		{
			if(ancestors.back()->right == t)
			{
				p = &(ancestors.back()->right);
			}
			else
			{
				p = &(ancestors.back()->left);
			}
		}
		balance(t, p);
	}//update the height and balance the ancestors
}
void Tree::balance(Node *n, Node **p)
{
	int b_factor = balance_factor(n);
	Node *t;
	if(b_factor > 1)
	{
		t = n->left;
		if(balance_factor(t) == 1)
		{
			//cout << "left left " << *n << endl;
			n->left = t->right;
			t->right = n;
			*p = t;
		}//left left case
		else
		{
			//cout << "left right " << *n << endl;
			n->left = t->right;
			t->right = n->left->left;
			n->left->left = t;
			t->height = calc_height(t);
			t = n->left;
			n->left = t->right;
			t->right = n;
			*p = t;
		}//left right case
		n->height = calc_height(n);
		t->height = calc_height(t);
		(*p)->height = calc_height(*p);
	}
	else if(b_factor < -1)
	{
		t = n->right;
		if(balance_factor(n->right) == -1)
		{
			//cout << "right right " << *n << endl;
			n->right = t->left;
			t->left = n;
			*p = t;
		}//right right case
		else
		{
			//cout << "right left " << *n << endl;
			n->right = t->left;
			t->left = n->right->right;
			n->right->right = t;
			t->height = calc_height(t);
			t = n->right;
			n->right = t->left;
			t->left = n;
			*p = t;
		}//right left case
		n->height = calc_height(n);
		t->height = calc_height(t);
		(*p)->height = calc_height(*p);
	}
}

void Tree::insert(string name)
{
	Node *temp = new Node(name);
	insert(temp);
}

void Tree::print(Node *node)
{
	if(node != NULL)
	{
		if(node->left == NULL)
		{
			cout << node->height << " " << *node << endl;
			print(node->left);
		}
		else
		{
			print(node->left);
			cout << node->height << " " << *node << endl;
		}
		print(node->right);
	}
}

void Tree::print()
{
	print(root);
}

void Tree::export_arr(Node *node, vector<Node*> *ar)
{
	if(node != NULL)
		{
			if(node->left == NULL)
			{
				ar->push_back(node);
				export_arr(node->left, ar);
			}
			else
			{
				export_arr(node->left, ar);
				ar->push_back(node);
			}
			export_arr(node->right, ar);
		}
}

void Tree::export_arr(vector<Node*> *ar)
{
	export_arr(root, ar);
}

int Tree::calc_height(Node *n)
{
	if(n == NULL)
	{
		return 0;
	}
	else
	{
		int l,r;
		l = calc_height(n->left);
		r = calc_height(n->right);
		if(l > r)
		{
			return l + 1;
		}
		else
		{
			return r + 1;
		}
	}
}//recursively calculate the height of the node

int Tree::balance_factor(Node *n)
{
	if(n == NULL)
	{
		return 0;
	}
	int l_height, r_height;
	l_height = (n->left == NULL)?0:n->left->height;
	r_height = (n->right == NULL)?0:n->right->height;
	return l_height - r_height;
}
