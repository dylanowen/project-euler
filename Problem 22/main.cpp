#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include "CPUTimer.h"
#include "tree.h"

using namespace std;

int main(int argc, const char *argv[])
{
	CPUTimer ct;
	ifstream inf("names.txt");
	unsigned long long count = 0;
	int i = 1;
	vector<Node*> *ar = new vector<Node*>();
	vector<Node*>::iterator it;
	Tree *sort = new Tree();
	string name;
	inf.ignore(1);
	getline(inf, name, '"');
	sort->insert(name);
	while(getline(inf, name, ','))
	{
		inf.ignore(1);
		getline(inf, name, '"');
		sort->insert(name);
	}
	sort->export_arr(ar);
	for(it=ar->begin(); it < ar->end(); it++)
	{
		//cout << i << ": " << (*it)->get_score() << " * " << i << endl;
	   count += (*it)->get_score() * i++;
	}
	cout << "count = " << count << endl;
	cout << "CPU time: " << ct.cur_CPUTime() << endl;
	return 0;
}
