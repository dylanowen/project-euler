#pragma once

#include <string>
#include <iostream>

using namespace std;

class Node
{
		string name;
		int score;

		int height;
		Node *left;
		Node *right;

		void str_score();

	public:
		Node();
		Node(string nn);
		~Node();

		void set(string nn);
		const string get_name();
		const int get_score();
		Node& operator=(string nn);
		friend ostream& operator<<(ostream &os, const Node n);
		friend class Tree;
};
