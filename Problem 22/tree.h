#pragma once

#include <iostream>
#include <cstdlib>
#include <list>
#include <vector>
#include <string>
#include "node.h"

#define LEFT 0
#define RIGHT 1

using namespace std;

class Node;

class Tree
{
	private:
		Node *root;
		int calc_height(Node *n);
		int balance_factor(Node *n);
	public:
		Tree();
		~Tree();
		void insert(Node *nn);
		void insert(string name);
		void balance(Node *node, Node **p);
		void print();
		void print(Node *node);
		void export_arr(vector<Node*> *ar);
		void export_arr(Node *node, vector<Node*> *ar);
};
