#include "node.h"

using namespace std;

Node::Node()
{
	set("");
	left = NULL;
	right = NULL;
	height = 1;
}
Node::Node(string nn)
{
	set(nn);
	left = NULL;
	right = NULL;
	height = 1;
}
Node::~Node(){}

void Node::set(string nn)
{
	name = nn;
	str_score();
}
const string Node::get_name()
{
	return name;
}
const int Node::get_score()
{
	return score;
}
Node& Node::operator=(string nn)
{
	set(nn);
	return *this;
}
ostream& operator<<(ostream &os, const Node n)
{
	os << n.name << "," << n.score;
	return os;
}

void Node::str_score()
{
	unsigned int i;
	score = 0;
	for(i = 0; i < name.length(); i++)
	{
		if(name[i] >= 'A' && name[i] <= 'Z')
		{
			score += (int) name[i] - 'A' + 1;
		}
		else if(name[i] >= 'a' && name[i] <= 'z')
		{
			score += (int) name[i] - 'a' + 1;
		}
	}
}
