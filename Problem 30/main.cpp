#include <iostream>

using namespace std;

unsigned pow(unsigned long long num, unsigned power)
{
	unsigned long long answer = num;
	for(unsigned i = 1; i < power; i++)
	{
		answer *= num;
	}

	return answer;
}

int main(int argc, const char *argv[])
{
	unsigned long long temp, sum, total = 0;

	for(unsigned i = 2; i <= 354294; i++)
	{
		temp = i;
		sum = 0;
		while(temp > 0)
		{
			sum += pow(temp % 10, 5);
			//cout << temp % 10 << ": " << sum << endl;
			temp /= 10;
		}

		if(sum == i)
		{
			cout << i << endl;
			total += sum;
		}
	}

	cout << "Total: " << total << endl;

	return 0;
}//main()
