#include <cstdio>
#include "CPUTimer.h"

using namespace std;

int sequence(long num);

int main(int argc, const char *argv[])
{
	CPUTimer ct;
	int i, t, seq = 0, answer = 0;
	for(i = 1; i < 1000000; i++)
	{
		t = sequence((long) i);
		if(t > seq)
		{
			seq = t;
			answer = i;
		}
	}
	cout << answer << " - " << seq << endl;
	cout << "CPU time: " << ct.cur_CPUTime() << endl;
}//main()

int sequence(long num)
{
	int count = 0;
	while(num != 1)
	{
		if(num % 2 == 0)
		{
			num = num / 2;
		}
		else
		{
			num = 3 * num + 1;
		}
		count++;
	}
	return count;
}
