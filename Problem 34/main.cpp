#include <cstdio>
#include <iostream>
#include <string>
#include <sstream>

#include "Timer.h"

using namespace std;

unsigned int sumFactorialDigits(unsigned int num);
unsigned int factorial(unsigned int num);

int main(int argc, const char *argv[])
{	
	Timer *ct;
	ct = new Timer();
	
	int sum = 0;
	
	for(unsigned int i = 3; i < 2540160; i++) //found this number from wolfram alpha 9! + 9! + 9! + 9! + 9! + 9! + 9!
	{
		if(i == sumFactorialDigits(i))
		{
			cout << i << endl;
			sum += i;
		}
	}
	
	
	
	cout << "Number: " << sum << endl;
	
	cout << "Ran in REAL: " << ct->format(ct->timeReal()) << " CPU: " << ct->timeCPU() << endl;
	delete ct;
	return 1;
}//main()

unsigned int sumFactorialDigits(unsigned int num)
{
	stringstream ss;
	string str;
	unsigned int sum = 0;
	
	ss << num;
	str = ss.str();
	for(unsigned int i = 0; i < str.length(); i++)
	{
		sum += factorial(str[i] - '0');
	}
	
	return sum;
}

unsigned int factorial(unsigned int num)
{
	unsigned int sum = 1;
	if(num == 0)
	{
		return 1;
	}
	for(unsigned int i = 1; i <= num; i++)
	{
		sum *= i;
	}
	
	return sum;
}