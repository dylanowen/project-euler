#include <cstdio>
#include "CPUTimer.h"

using namespace std;

int main(int argc, const char *argv[])
{
	CPUTimer ct;
	unsigned long long sum_o_sqr = 0, sqr_o_sum = 0;
	int i, temp;
	for(i = 1; i <= 100; i++)
	{
		sum_o_sqr += i * i;
		temp += i; 
	}
	sqr_o_sum = temp * temp;
	cout << sqr_o_sum - sum_o_sqr << endl;
	cout << "CPU time: " << ct.cur_CPUTime() << endl;
}//main()