#include <cstdio>
#include <iostream>

#include "Timer.h"

using namespace std;

struct array{
  unsigned int length;
  unsigned int* data;
};

array findPrimes(unsigned int upTo);

int main(int argc, const char *argv[])
{
	array primes;
	Timer *ct;
	ct = new Timer();
	
	primes = findPrimes(2000000);
	
	cout << "Total primes: " << primes.length << endl;
	cout << "10,001st prime: " << primes.data[10000] << endl;
	
	cout << "Ran in REAL: " << ct->format(ct->timeReal()) << " CPU: " << ct->timeCPU() << endl;
	delete ct;
	return 1;
}//main()

array findPrimes(unsigned int upTo)
{
	unsigned int j, count = 0;
	bool *temp = new bool[upTo];
	array primes;
	
	for(unsigned int i = 0; i < upTo; i++)
	{
		temp[i] = true;
	}
	
	temp[0] = false; //0 isn't prime
	temp[1] = false; //1 isn't prime
	
	for(unsigned int i = 2; i < ((upTo / 2) + 1); i++)
	{
		if(temp[i])
		{
			j = i + i;
			while(j < upTo)
			{
				temp[j] = false;
				j += i;
			}
		}
	}
	
	for(unsigned int i = 0; i < upTo; i++)
	{
		if(temp[i])
		{
			count++;
		}
	}
	
	primes.length = count;
	primes.data = new unsigned int[count];
	j = 0;
	
	for(unsigned int i = 0; i < upTo; i++)
	{
		if(temp[i])
		{
			primes.data[j] = i;
			j++;
		}
	}
	
	return primes;
}