#include "superlong.h"

using namespace std;

SuperLong::SuperLong()
{
	number.resize(0);
	sign = false;
	number.push_back(0);
}
	
SuperLong::SuperLong(long long num)
{
	number.resize(0);
	sign = false;
	if(num == 0)
	{
		number.push_back(0);
	}
	else
	{
		if(num < 0)
		{
			sign = true;
			num = -num;
		}
		bool start = false;
		long long t, count = 1000000000000000000;
		vector<char> temp;
		int i;
		while(count > 0)
		{
			t = num / count;
			if(t > 0)
			{
				start = true;
				num -= t * count;
			}
			if(start)
			{
				temp.push_back(t);
			}
			count /= 10;
		}
		for(i = temp.size() - 1; i >= 0; i--)
		{
			number.push_back(temp[i]);
		}
	}
}

SuperLong::SuperLong(unsigned long long num)
{
	number.resize(0);
	sign = false;
	if(num == 0)
	{
		number.push_back(0);
	}
	else
	{
		bool start = false;
		unsigned long long t, count = 10000000000000000000U;
		vector<char> temp;
		int i;
		while(count > 0)
		{
			t = num / count;
			if(t > 0)
			{
				start = true;
				num -= t * count;
			}
			if(start)
			{
				temp.push_back(t);
			}
			count /= 10;
		}
		for(i = temp.size() - 1; i >= 0; i--)
		{
			number.push_back(temp[i]);
		}
	}
}

SuperLong::SuperLong(string num)
{
	int i;
	int t;
	number.resize(0);
	sign = false;
	if(num.size() > 0)
	{
		if(num[0] == '-')
			sign = true;
		for(i = num.size() - 1; i >= 0; i--)
		{
			t = num[i] - '0';
			if(t < 10 && t >= 0)
				number.push_back(t);
		}
	}
}

bool SuperLong::operator==(const SuperLong &rhs) const
{
	if((this->number.size() != rhs.number.size()) || (this->sign != rhs.sign))
		return false;
	else
	{
		bool t = true;
		unsigned int i;
		for(i = 0; i < this->number.size(); i++)
		{
			if(this->number[i] != rhs.number[i])
				t = false;
		}
		return t;
	}
}

bool SuperLong::operator!=(const SuperLong &rhs) const
{
	return !(*this == rhs);
}

bool SuperLong::operator<(const SuperLong &rhs) const
{
	bool t = true;
	int i;
	if(this->number.size() < rhs.number.size())
		return true;
	else if(this->number.size() > rhs.number.size())
		return false;
	else if(this->sign && !rhs.sign)
		return true;
	else if(!this->sign && rhs.sign)
		return false;
	else if(this->sign && rhs.sign)
	{
		for(i = this->number.size(); i >= 0 && t; i--)
		{
			if(this->number[i] <= rhs.number[i])
			{
				t = false;
			}
		}
	}
	else
	{
		for(i = this->number.size(); i <= 0 && t; i--)
		{
			if(this->number[i] >= rhs.number[i])
			{
				t = false;
			}
		}
	}
	return (t && (*this != rhs));
}

bool SuperLong::operator>(const SuperLong &rhs) const 
{
	return (!(*this < rhs) && !(*this == rhs));
}

bool SuperLong::operator<=(const SuperLong &rhs) const
{
	return (*this == rhs || *this < rhs);
}

bool SuperLong::operator>=(const SuperLong &rhs) const
{
	return (*this == rhs || *this > rhs);
}

const SuperLong SuperLong::operator+(const SuperLong &rhs) const
{
	SuperLong *t = new SuperLong();
	t->number.resize(0);
	unsigned int i;
	int n, r, temp, carry = 0;
	if(this->sign && !rhs.sign)
	{
		t->number = this->number;
		if(rhs > *t)
		{
			*t = rhs - *t;
			t->sign = false;
		}
		else if(*t > rhs)
		{
			*t = *t - rhs;
			t->sign = true;
		}
		else
		{
			*t = (long long) 0;
		}
	}
	else if(!this->sign && rhs.sign)
	{
		t->number = rhs.number;
		if(*t < rhs)
		{
			*t = *t - rhs;
			t->sign = true;
		}
		else if(*t > rhs)
		{
			*t = rhs - *t;
			t->sign = false;
		}
		else
		{
			*t = (long long) 0;
		}
	}
	else
	{
		t->sign = this->sign;
		for(i = 0; i < this->number.size() || i < rhs.number.size() || carry > 0; i++)
		{
			n = (i >= this->number.size())?0:this->number[i];
			r = (i >= rhs.number.size())?0:rhs.number[i];
			temp = n + r + carry;
			carry = 0;
			if(temp > 9)
			{
				temp -= 10;
				carry = 1;
			}
			t->number.push_back(temp);
		}
	}
	return *t;
}

const SuperLong SuperLong::operator-(const SuperLong &rhs) const
{
	SuperLong *t = new SuperLong();
	t->number.resize(0);
	unsigned int i;
	int n, r, temp, carry = 0;
	if(!this->sign && rhs.sign)
	{
		t->number = rhs.number;
		*t = *this + *t;
		t->sign = false;
	}
	/*
	else if(!this->sign && rhs.sign)
	{
		t->number = rhs.number;
		if(*t < rhs)
		{
			*t = *t - rhs;
			t->sign = true;
		}
		else if(*t > rhs)
		{
			*t = rhs - *t;
			t->sign = false;
		}
		else
		{
			*t = 0;
		}
	}
	else
	{
		t->sign = this->sign;
		for(i = 0; i < this->number.size() || i < rhs.number.size() || carry > 0; i++)
		{
			n = (i >= this->number.size())?0:this->number[i];
			r = (i >= rhs.number.size())?0:rhs.number[i];
			temp = n + r + carry;
			carry = 0;
			if(temp > 9)
			{
				temp -= 10;
				carry = 1;
			}
			t->number.push_back(temp);
		}
	}*/
	return *t;
}

const SuperLong SuperLong::operator*(const SuperLong &rhs) const
{
	SuperLong *t = new SuperLong(), *temp = new SuperLong();
	t->number.resize(0);
	if(*this == *(new SuperLong((long long) 0)) || rhs == *(new SuperLong((long long) 1)))
	{
		*t = (long long) 0;
	}
	else
	{
		int i, j, k, carry, tmp;
		for(i = 0; i < (int) rhs.number.size(); i++)
		{
			carry = 0;
			temp->number.resize(0);
			for(k = 0; k < i; k++)
			{
				temp->number.push_back(0);
			}
			for(j = 0; j < (int) this->number.size(); j++)
			{
				tmp = this->number[j] * rhs.number[i] + carry;
				carry = tmp / 10;
				temp->number.push_back(tmp % 10);
			}
			if(carry != 0)
			{
				temp->number.push_back(carry);
			}
			*t += *temp;
		}
	}
	t->sign = ((this->sign && !rhs.sign) || (!this->sign && rhs.sign))?true:false;
	return *t;
}

const SuperLong& SuperLong::operator++()
{
	*this = *this + *(new SuperLong((long long) 1));
	return *this;
}
const SuperLong SuperLong::operator++(int)
{
	SuperLong temp = *this;
	++(*this);
	return temp;
}
const SuperLong& SuperLong::operator--()
{
	*this = *this - *(new SuperLong((long long) 1));
	return *this;
}
const SuperLong SuperLong::operator--(int)
{
	SuperLong temp = *this;
	--(*this);
	return temp;
}
SuperLong& SuperLong::operator+=(const SuperLong &rhs)
{
	*this = *this + rhs;
	return *this;
}

SuperLong& SuperLong::operator-=(const SuperLong &rhs)
{
	*this = *this - rhs;
	return *this;
}

SuperLong& SuperLong::operator*=(const SuperLong &rhs)
{
	*this = *this * rhs;
	return *this;
}		
		
SuperLong& SuperLong::operator=(const SuperLong &rhs)
{
	if(this == &rhs)
		return *this;
	this->number = rhs.number;
	this->sign = rhs.sign;
	return *this;
}

ostream& operator<<(ostream &os, const SuperLong &super)
{
	int i;
	if(super.sign)
		os << "-";
	for(i = super.number.size() - 1; i >= 0; i--)
	{
		os << (int) super.number[i];
	}
	return os;
}