#include <cstdio>
#include <iostream>
#include <string>
#include <sstream>

#include "SuperLong.h"
#include "Timer.h"

using namespace std;

int main(int argc, const char *argv[])
{
	Timer *ct;
	ct = new Timer();

	stringstream ss;
	string str;
	SuperLong *number = new SuperLong((long long) 1), *temp = new SuperLong((long long) 2), *sum = new SuperLong((long long) 0);
	
	for(int i = 0; i < 1000; i++)
	{
		*number *= *temp;
		cout << *number << endl;
	}
	
	ss << *number;
	str = ss.str();
	for(int i = 0; i < str.length(); i++)
	{
		delete temp;
		temp = new SuperLong((long long) (str[i] - '0'));
		cout << *temp << " ";
		*sum += *temp;
		//cout << *sum << endl;
	}
	
	cout << "Sum: " << *sum << endl;
	
	cout << "Ran in REAL: " << ct->format(ct->timeReal()) << " CPU: " << ct->timeCPU() << endl;
	delete ct;
	return 1;
}//main()