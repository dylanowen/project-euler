#include <cstdio>
#include <iostream>
#include <fstream>
#include <cstdlib>

#include "Timer.h"

using namespace std;

void printTriangle(unsigned int** triangle, unsigned int length);
void myGetLine(ifstream* file, char string[], unsigned int length, char delims[], unsigned int delimLen);

int main(int argc, const char *argv[])
{
	Timer *ct;
	ct = new Timer();
	
	char temp[4];
	char delims[2] = {'\n', ' '};
	unsigned int length;
	unsigned int** triangle;
	ifstream *triangleFile;
	triangleFile = new ifstream("triangle.txt", ios::in);
	
	myGetLine(triangleFile, temp, 4, delims, 2);
	length = atoi(temp);
	triangle = new unsigned int*[length];

	for(unsigned int i = 0; i < length; i++)
	{
		triangle[i] = new unsigned int[(i + 1)];
		for(unsigned int j = 0; j < (i + 1); j++)
		{
			myGetLine(triangleFile, temp, 4, delims, 2);
			triangle[i][j] = atoi(temp);
		}
	}
	
	for(int i = (length - 2); i >= 0; i--)
	{
		for(unsigned int j = 0; j < (i + 1); j++)
		{
			if(triangle[i + 1][j] > triangle[i + 1][j + 1])
			{
				triangle[i][j] += triangle[i + 1][j];
			}
			else
			{
				triangle[i][j] += triangle[i + 1][j + 1];
			}
		}
	}
	
	cout << "triangle numbers: " << triangle[0][0] << endl; 
	
	for(unsigned int i = 0; i < length; i++)
	{
		delete [] triangle[i];
	}
	delete [] triangle;

	cout << "Ran in " << ct->format(ct->timeReal()) << endl;
	delete ct;
	return 1;
}//main()

void printTriangle(unsigned int** triangle, unsigned int length)
{
	for(unsigned int i = 0; i < length; i++)
	{
		for(unsigned int j = 0; j < (i + 1); j++)
		{
			cout << triangle[i][j] << " ";
		}
		cout << endl;
	}
}

void myGetLine(ifstream* file, char string[], unsigned int length, char delims[], unsigned int delimLen)
{
	bool test = true;
	char temp;
	unsigned int count = 0; 
	
	while(test && count < length && file->good())
	{
		temp = file->get();
		if(!file->good())
		{
			test = false;
		}
		else
		{
			for(unsigned int i = 0; i < delimLen; i++)
			{
				//cout << (int) temp << " =? " << (int) delims[i] << endl;
				if(temp == delims[i])
				{
					test = false;
				}
			}
		}
		if(test)
		{
			string[count] = temp;
			//cout << "\"" << string[count] << "\"" << endl;
			count++;
		}
	}
	if(count < length)
	{
		string[count] = '\0';
	}
	else
	{
		string[(length - 1)] = '\0';
	}
}