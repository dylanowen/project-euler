#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <fstream>

#include "Timer.h"

using namespace std;

struct Triangle
{
	int ax;
	int ay;
	int bx;
	int by;
	int cx;
	int cy;
};

bool interiorOrigin(Triangle *tri);
bool aboveLine(double slope, double yIntercept, double x, double y);

int main(int argc, const char *argv[])
{
	Timer *ct;
	ct = new Timer();
	
	char temp[10];
	Triangle *tri;
	unsigned int counter = 0;
	unsigned int i = 1;
	
	ifstream *triFile;
	triFile = new ifstream("triangles.txt", ios::in);
	while(triFile->good())
    {
		tri = new Triangle;
		triFile->getline(temp, 10, ',');
		tri->ax = atoi(temp);
		triFile->getline(temp, 10, ',');
		tri->ay = atoi(temp);
		triFile->getline(temp, 10, ',');
		tri->bx = atoi(temp);
		triFile->getline(temp, 10, ',');
		tri->by = atoi(temp);
		triFile->getline(temp, 10, ',');
		tri->cx = atoi(temp);
		triFile->getline(temp, 10);
		tri->cy = atoi(temp);
		cout << i++ << ": " << endl;
		if(interiorOrigin(tri))
		{
			cout << "\ttrue" << endl;
			counter++;
		}
		else
		{
			cout << "\tfalse" << endl;
		}
		delete tri;
    }
	triFile->close();
	
	cout << "Sum: " << counter << endl;
	
	cout << "Ran in " << ct->format(ct->timeReal()) << endl;
	delete ct;
	return 1;
}//main()

bool interiorOrigin(Triangle *tri)
{
	double slopeAB, slopeAC, slopeBC, interAB, interAC, interBC;
	double centroidX, centroidY;
	centroidX = ((double) tri->ax + tri->bx + tri->cx) / 3;
	centroidY = ((double) tri->ay + tri->by + tri->cy) / 3;
	
	slopeAB = ((double) (tri->ay - tri->by)) / ((double) (tri->ax - tri->bx));
	slopeAC = ((double) (tri->ay - tri->cy)) / ((double) (tri->ax - tri->cx));
	slopeBC = ((double) (tri->by - tri->cy)) / ((double) (tri->bx - tri->cx));
	interAB = (-tri->bx * slopeAB) + tri->by;
	interAC = (-tri->cx * slopeAC) + tri->cy;
	interBC = (-tri->cx * slopeBC) + tri->cy;
	
	//cout << "(" << centroidX << "," << centroidY << ") y=" << slopeAB << "x+" << interAB << " y=" << slopeAC << "x+" << interAC << " y=" << slopeBC << "x+" << interBC << endl;
	
	//cout << aboveLine(slopeAB, interAB, centroidX, centroidY) << " == " << aboveLine(slopeAB, interAB, 0, 0) << endl;
	if(aboveLine(slopeAB, interAB, centroidX, centroidY) == aboveLine(slopeAB, interAB, 0, 0))
	{
		//cout << aboveLine(slopeAC, interAC, centroidX, centroidY) << " == " << aboveLine(slopeAC, interAC, 0, 0) << endl;
		if(aboveLine(slopeAC, interAC, centroidX, centroidY) == aboveLine(slopeAC, interAC, 0, 0))
		{
			//cout << aboveLine(slopeBC, interBC, centroidX, centroidY) << " == " << aboveLine(slopeBC, interBC, 0, 0) << endl;
			if(aboveLine(slopeBC, interBC, centroidX, centroidY) == aboveLine(slopeBC, interBC, 0, 0))
			{
				return true;
			}
		}
	}
	
	return false;
}

bool aboveLine(double slope, double yIntercept, double x, double y)
{
	cout << "y=" << slope << "x+" << yIntercept << " (" << x << "," << y << ")" << endl;
	if(y > (slope * x + yIntercept))
	{
		return true;
	}
	return false;
}