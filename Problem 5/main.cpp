#include <cstdio>
#include "CPUTimer.h"

using namespace std;

int main(int argc, const char *argv[])
{
	CPUTimer ct;
	unsigned int i = 20;
	while(i % 20 != 0 || i % 19 != 0 || i % 18 != 0 || i % 17 != 0 || i % 16 != 0 || i % 15 != 0 || i % 14 != 0 || i % 13 != 0 || i % 12 != 0 || i % 11 != 0)
	{
		i++;
	}
	cout << i << endl;
	cout << "CPU time: " << ct.cur_CPUTime() << endl;
}//main()