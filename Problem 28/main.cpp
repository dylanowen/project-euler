#include <cstdio>
#include "CPUTimer.h"

using namespace std;

int main(int argc, const char *argv[])
{
	CPUTimer ct;
	int i, temp = 4, inc = 0, total = 0;
	for(i = 1; i <= (1001 * 1001); i += inc, temp++)
	{
		total += i;
		if(temp == 4)
		{
			inc += 2;
			temp = 0;
		}
	}
	cout << total << endl;
	cout << "CPU time: " << ct.cur_CPUTime() << endl;
}//main()