#include <cstdio>
#include <iostream>
#include <fstream>
#include <string>

#include "SuperLong.h"
#include "Timer.h"

using namespace std;

int main(int argc, const char *argv[])
{
	Timer *ct;
	ct = new Timer();
	
	char ctemp[256];
	string str;
	ifstream *wordFile;
	SuperLong sum, temp;
	wordFile = new ifstream("numbers.txt", ios::in);
	
	while(wordFile->good())
    {
		wordFile->getline(ctemp, 256);
		str = ctemp;
		temp = *(new SuperLong(str));
		sum += temp;
		cout << sum << endl;
    }
	
	cout << "Number: " << sum << endl;
	
	cout << "Ran in REAL: " << ct->format(ct->timeReal()) << " CPU: " << ct->timeCPU() << endl;
	delete ct;
	return 1;
}//main()