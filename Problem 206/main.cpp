#include <iostream>
#include "Timer.h"

#include <cmath>
#include <string>
#include <sstream>

using namespace std;

int main(int argc, const char *argv[])
{
	Timer *ct;
	ct = new Timer();
	
	stringstream *ss;
	string str;
	unsigned long long temp;
	unsigned long long lowerBound = (unsigned long long) sqrt(1000000000000000000) - 1;
	unsigned long long upperBound = (unsigned long long) sqrt(1929394959697989990) + 1;
	unsigned long long progress = (upperBound - lowerBound) / 10;
	unsigned long long progressTemp = progress + lowerBound;
	int progressNum = 10;
	
	cout << "lowerBound=" << lowerBound << " upperBound=" << upperBound << " progress=" << progress << endl;
	
	for(unsigned long long i = lowerBound; i <= upperBound; i++)
	{
		if(i == progressTemp)
		{
			cout << progressNum << "%" << endl;
			progressTemp += progress;
			progressNum += 10;
		}
		temp = i*i;
		ss = new stringstream();
		*ss << temp;
		str = ss->str();
		if(str.length() == 19 && str[0] == '1' && str[2] == '2' && str[4] == '3' && str[6] == '4' && str[8] == '5' && str[10] == '6' && str[12] == '7' && str[14] == '8' && str[16] == '9' && str[18] == '0')
		{
			cout << "Answer: " << i << endl; 
			break;
		}
		delete ss;
	}

	cout << "Ran in REAL: " << ct->format(ct->timeReal()) << " CPU: " << ct->timeCPU() << endl;
	delete ct;
}//main()