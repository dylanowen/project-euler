//I somewhat cheated on this one by multithreading everything so it would finish faster

#include <cstdio>
#include <iostream>
#include <vector>
#include <cmath>
#include <climits>
#include <pthread.h>

#include "Timer.h"

using namespace std;

vector<int>* findDivisors(unsigned long long number);
void mainPart1(void *);
void mainPart2(void *);

pthread_mutex_t locker = PTHREAD_MUTEX_INITIALIZER;

bool *primes;
unsigned long long answer1;
unsigned long long answer2;
unsigned int end1 = 60000000;
unsigned int end2 = 100000000;
unsigned int upTo;

int main(int argc, const char *argv[])
{
	Timer *ct;
	ct = new Timer();
	
	unsigned long long sum = 0;
	answer1 = answer2 = 0;	

	upTo = end2 + 100;
	primes = new bool[upTo];
	
	for(unsigned int i = 0; i < upTo; i++)
	{
		primes[i] = true;
	}
	
	primes[0] = false; //0 isn't prime
	primes[1] = false; //1 isn't prime
	
	unsigned int j;
	for(unsigned int i = 2; i < ((upTo / 2) + 1); i++)
	{
		if(primes[i])
		{
			j = i + i;
			while(j < upTo)
			{
				primes[j] = false;
				j += i;
			}
		}
	}
	
	cout << "Calculated Primes in " << ct->timeCPU() << endl;
	
	pthread_t thread1, thread2;
	pthread_create(&thread1, NULL, (void* (*)(void*))&mainPart1, NULL);
	pthread_create(&thread2, NULL, (void* (*)(void*))&mainPart2, NULL);

	pthread_join(thread1, NULL);
	pthread_join(thread2, NULL);
	
	sum = answer1 + answer2;
	
	cout << "Number: " << sum << endl;
	
	delete primes;
	cout << "Ran in REAL: " << ct->format(ct->timeReal()) << " CPU: " << ct->timeCPU() << endl;
	delete ct;
	return 1;
}//main()

void mainPart1(void *)
{
	bool test;
	unsigned int temp;
	vector<int> *divisors;
	for(unsigned int i = 1; i < end1; i++)
	{
		test = true;
		if(primes[i + 1])
		{
			divisors = findDivisors(i);
			for(unsigned long long j = 0; j < divisors->size(); j++)
			{
				temp = (i / (*divisors)[j]) + (*divisors)[j];
				if(temp >= upTo)
				{
					pthread_mutex_lock(&locker);
					cout << "Huge Error" << endl;
					pthread_mutex_unlock(&locker);
				}
				if(!primes[temp])
				{
					//cout << (*divisors)[j] << "=" << temp << " ";
					test = false;
					break;
				}
			}
			delete divisors;
			if(test)
			{
				answer1 += i;
			}
		}
	}
	
	pthread_mutex_lock(&locker);
	cout << "Answer1 = " << answer1 << endl;
	pthread_mutex_unlock(&locker);
}

void mainPart2(void *)
{
	bool test;
	unsigned int temp;
	vector<int> *divisors;
	for(unsigned int i = end1; i <= end2; i++)
	{
		test = true;
		if(primes[i + 1])
		{
			divisors = findDivisors(i);
			for(unsigned long long j = 0; j < divisors->size(); j++)
			{
				temp = (i / (*divisors)[j]) + (*divisors)[j];
				if(temp >= upTo)
				{
					pthread_mutex_lock(&locker);
					cout << "Huge Error" << endl;
					pthread_mutex_unlock(&locker);
				}
				if(!primes[temp])
				{
					test = false;
					break;
				}
			}
			delete divisors;
			if(test)
			{
				answer2 += i;
			}
		}
	}
	
	pthread_mutex_lock(&locker);
	cout << "Answer2 = " << answer2 << endl;
	pthread_mutex_unlock(&locker);
}

vector<int>* findDivisors(unsigned long long number)
{
	vector<int> *divisors = new vector<int>();
	vector<int> *temp = new vector<int>();
	int tempval;
	bool test;
	
	for(unsigned long long i = 1; i <= ((int) sqrt(number) + 1); i++)
	{
		if(number % i == 0)
		{
			divisors->push_back(i);
		}
	}
	
	*temp = *divisors;
	for(unsigned long long i = 0; i < temp->size(); i++)
	{
		tempval = number / (*temp)[i];
		test = true;
		for(unsigned long long j = 0; j < temp->size(); j++)
		{
			if((*temp)[j] == tempval)
			{
				test = false;
				break;
			}
		}
		if(test)
		{
			divisors->push_back(tempval);
		}
	}

	delete temp;
	return divisors;
}