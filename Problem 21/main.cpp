#include <cstdio>
#include <iostream>

#include "Timer.h"

using namespace std;

unsigned int sumDivisors(unsigned long long number);

int main(int argc, const char *argv[])
{
	unsigned int temp, count = 0;
	Timer *ct;
	ct = new Timer();
	
	for(unsigned int i = 0; i < 10000; i++)
	{
		temp = sumDivisors(i);
		if(i != temp && sumDivisors(temp) == i)
		{
			count += temp;
			cout << i << " " << temp << endl;
		}
	}
	
	cout << "Sum: " << count << endl;
	
	cout << "Ran in REAL: " << ct->format(ct->timeReal()) << " CPU: " << ct->timeCPU() << endl;
	delete ct;
	return 1;
}//main()

unsigned int sumDivisors(unsigned long long number)
{

	int count = 0;
	
	for(unsigned long long i = 1; i <= ((number / 2) + 1); i++)
	{
		if(number % i == 0)
		{
			count += i;
		}
	}
	
	return count;
}