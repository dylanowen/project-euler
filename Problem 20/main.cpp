#include <cstdio>
#include <iostream>
#include <string>
#include <sstream>

#include "SuperLong.h"
#include "Timer.h"

using namespace std;

int main(int argc, const char *argv[])
{
	Timer *ct;
	ct = new Timer();

	stringstream ss;
	string str;
	SuperLong *number = new SuperLong((long long) 1), *temp;
	int sum;
	
	cout << *number << endl;
	
	for(int i = 1; i <= 100; i++)
	{
		temp = new SuperLong((long long) i);
		*number *= *temp;
		cout << *temp << " " << *number << endl;
	}
	
	ss << *number;
	str = ss.str();
	for(unsigned int i = 0; i < str.length(); i++)
	{
		sum += str[i] - '0';
	}
	
	cout << "Sum: " << sum << endl;
	
	cout << "Ran in REAL: " << ct->format(ct->timeReal()) << " CPU: " << ct->timeCPU() << endl;
	delete ct;
	return 1;
}//main()