#include <cstdio>
#include <string>
#include <sstream>
#include "CPUTimer.h"

using namespace std;

bool check(int num);

int main(int argc, const char *argv[])
{
	CPUTimer ct;
	int i, j, temp, answer = 0;
	for(i = 100; i < 1000; i++)
	{
		for(j = 100; j < 1000; j++)
		{
			temp = i*j;
			if(check(temp) && temp > answer)
			{
				answer = temp;
			}
		}
	}
	cout << answer << endl;
	cout << "CPU time: " << ct.cur_CPUTime() << endl;
}//main()

bool check(int num)
{
	bool t = true;
	unsigned int i;
	string s;
	stringstream ss;
	ss << num;
	s = ss.str();
	if(s.size() % 2 == 0)
	{
		for(i = 0; i < s.size() / 2; i++)
		{
			if(s[i] != s[s.size() - 1 - i])
			{
				t = false;
			}
		}
		return t;
	}
	return false;
}