#include <iostream>
#include <cstdlib>
//#include <string>
//#include "num.h"

#define LEN 3
#define NUM 0

using namespace std;

void print(int* answer, int* temp);
void del(int* ar, int num);
int fact(int num);

int main(int argc, const char *argv[])
{
	int i, t, num, answer[10], temp[10];
	if(argc == 2)
	{
		num = atoi(argv[1]) - 1;
	}
	else
	{
		num = 10;
	}
	for(i = 0; i < 10; i++)
	{
		answer[i] = -1;
		temp[i] = i;
	}
	
	
	for(i = 0; i < 10; i++)
	{
		cout << i;
		int change = 0, count, f;
		count = 10 - i - 1;
		f = fact(count);
		if(num >= f && f > 0)
		{
			if(num >= (count * f))
			{
				cout << " num >= (count * f) ";
				change = count;
				num = num - (count * f);
			}
			else
			{
				cout << " num < (count * f) ";
				t = num % f;
				change = num / f;
				num = t;
			}
		}
		cout << " num = " << num << " count=" << count << " f=" << f;
		cout << " change = " << change;
		answer[i] = temp[0 + change];
		del(temp, answer[i]);
		print(answer, temp);
		cout << endl;
	}
}//main()

void print(int* answer, int* temp)
{
	int i;
	cout << endl << "answer = ";
	for(i = 0; i < 10; i++)
	{
		if(answer[i] != -1)
			cout << answer[i];
	}
	cout << endl << "temp = ";
	for(i = 0; i < 10; i++)
	{
		if(temp[i] != -1)
			cout << temp[i];
	}
	cout << endl;
}

void del(int* ar, int num)
{
	int i, j, temp[10];
	for(i = 0; i < 10; i++)
	{
		temp[i] = -1;
	}
	for(i = j = 0; i < 10; i++)
	{
		if(ar[i] != num)
		{
			temp[j] = ar[i];
			j++;
		}
	}
	for(i = 0; i < 10; i++)
	{
		ar[i] = temp[i];
	}
}

int fact(int num)
{
	if(num < 1)
	{
		return 0;
	}
	if(num == 1)
	{
		return 1;
	}
	return num * fact(num - 1);
}