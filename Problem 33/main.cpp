#include <cstdio>
#include <iostream>

#include "Timer.h"

using namespace std;

int main(int argc, const char *argv[])
{
	int iActual, jActual;
	double aDiv, tDiv;
	Timer *ct;
	ct = new Timer();
	
	for(unsigned int i1 = 1; i1 < 10; i1++)
	{
		for(unsigned int i2 = 1; i2 < 10; i2++)
		{
			for(unsigned int j1 = 1; j1 < 10; j1++)
			{
				for(unsigned int j2 = 1; j2 < 10; j2++)
				{
					if(!(i1 == j1 && i2 == j2))
					{
						iActual = ((i1 * 10) + i2);
						jActual = ((j1 * 10) + j2);
						aDiv = (double) iActual / (double) jActual;
						if(aDiv < 1)
						{
							tDiv = (double) i1 / (double) j1;
							if(i2 == j2 && aDiv == tDiv)
							{
								cout << iActual << "/" << jActual << " = " << i1 << "/" << j1 << " " << aDiv << endl;
							}
							
							tDiv = (double) i1 / (double) j2;
							if(i2 == j1 && aDiv == tDiv)
							{
								cout << iActual << "/" << jActual << " = " << i1 << "/" << j2 << " " << aDiv << endl;
							}
							
							tDiv = (double) i2 / (double) j1;
							if(i1 == j2 && aDiv == tDiv)
							{
								cout << iActual << "/" << jActual << " = " << i2 << "/" << j1 << " " << aDiv << endl;
							}
							
							tDiv = (double) i2 / (double) j2;
							if(i1 == j1 && aDiv == tDiv)
							{
								cout << iActual << "/" << jActual << " = " << i2 << "/" << j2 << " " << aDiv << endl;
							}
						}
					}
				}
			}
		}
	}
	
	
	cout << "Ran in REAL: " << ct->format(ct->timeReal()) << " CPU: " << ct->timeCPU() << endl;
	delete ct;
	return 1;
}//main()